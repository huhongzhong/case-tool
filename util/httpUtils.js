// 正式服
export const baseUrl = 'https://api-pro.nftcn.com.cn/nms/dubbo';
import CryptoJS from '@/util/encryption/crypto-js/crypto-js';
import JSEncrypt from '@/util/encryption/jsencrypt/jsencrypt';
export default {
	baseUrl,
	// 请求方式 请求地址 	请求参数
	request(method, url, params) {
		let token = "";
		params = params || {}

		
		let time =new Date().getTime()
		let nftcnApiNonce = this.getNftcnApiNonce();
		let nftcnapiappsecret = CryptoJS.MD5('nftcn-web-h5' + nftcnApiNonce).toString().toUpperCase()
		let signTxt = this.getSignTxt({
			nftcnApiAppId: 'nftcn-web-h5',
			nftcnApiAppSecret: nftcnapiappsecret,
			nftcnApiNonce: nftcnApiNonce,
			nftcnApiTimestamp: time,
			...params
		})
		console.log(signTxt)
		let nftcnapisignature = this.getNftcnapisignature(signTxt)
		
		
		if (uni.getStorageSync("token")) {
			token =  uni.getStorageSync("token");
		}
		if(process.env.NODE_ENV==='development'){// 测试环境 打印日志
			console.log('发起请求,url:', url)
			console.log('发起请求,token:', token)
			console.log('发起请求,params:', JSON.stringify(params))
		}
		// uni.showLoading({
		// 	title: "数据加载中..."
		// })
		// resolve成功回调 reject失败回调
		return new Promise((resolve, reject) => {
			// Content-Type 请求内容格式
			uni.request({
				url: baseUrl + url,
				timeout: 30000,
				// header: {
				// 	'Content-Type': "application/json",
				// 	// "Authorization": token,
				// },
				header: {
					'Content-Type': "application/json",
					"Authorization":token,
					nftcnapiappid: 'nftcn-web-h5',
					nftcnapiappsecret: nftcnapiappsecret,
					nftcnApiNonce: nftcnApiNonce,
					nftcnapisignature: nftcnapisignature,
					nftcnApiTimestamp: time,
				},
				method,
				data:params,
				success(res) {
					if(process.env.NODE_ENV==='development'){// 测试环境 打印日志
						console.log('当前,url:', url)
						console.log('数据返回', JSON.stringify(res))
					}
					if (res.data.status.code == 0) {
						resolve(res.data)
					}else {// 成功的返回结构
						// uni.showModal({
						// 	title: '提示',
						// 	content: res.data.status.msg,
						// 	showCancel: false,
						// 	success() {
						// 		// uni.reLaunch({
						// 		// 	url:'/pages/index/index'
						// 		// })
						// 	}
						// })
						reject(res.data.status)
					}
				},
				fail(err) {
					reject(err.data)
				},
				complete() {
				}
			})

		})
	},
	// 获取21为随机数
	getNftcnApiNonce() {
		let str = ''
		for (let i = 0; i < 21; i++) {
			str += Math.round(Math.random() * 9) + ''
		}
		return str
	},
	getSignTxt(obj) {
		let e = obj
		const sortedKeys = Object.keys(obj).sort();
		var t = "";
		sortedKeys.forEach((function(a, i) {
			e[a].constructor === Object ? t += a + "=" + JSON.stringify(e[a]) + (i < sortedKeys
				.length - 1 ?
				"&" : "") : t += a + "=" + e[a] + (i < sortedKeys.length - 1 ? "&" : "")
		}))
		return t
	},
	getNftcnapisignature(plainText) {
		var signStr = new JSEncrypt();
		let privKey =
			'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCgeDeLV31/6mXaiBY2aFjwyomZbfcJ7x1cjGY6wenpta9vJ3z1vTbPxDoaz3wFMzwPG6Grl2eYIUjv4bvr3O6+//e0yOul7owzftx9vXIBS375nejn/XeR0HR0AnRRsuL6UKrmg7V/TmUS+e3fje7DmVo1AvoHSpO/o+SZemXlBExxYhLwXyDfW3Adv+dmZ7ybjObrnpwTS5qq1KNhlcDlbu3YpmD5igVR9Bez/jjqriM0MIeO3G0I4NC2lBr+QHsOvCi8f2n24vsRLsFQAm/+AUJNAyMSB0PUM3wwbTd+grUNlQXZtd1ByhuJZyWjpslfU4z6vy12UQaB2imDuQ3pAgMBAAECggEAATKEb5S6if7MvcGwML28lCdeuXdZlYhkNrGRfbS+sxC4+2JXC1pbE1fKezK7ISrNsuso3KfnjPoKmkeb6FmgqmoshfvNzlImV6gFqyaDuEDA9MnZ2AlLIBpnFAqEpatpCCcb71ZiP03tcSPOQ5HCi1EUnDqmdPF4gsCNTxvbsMbBaN3PPn6j7X8Tqi70d1dIAfnd/Ds9TtMSb1T7NWt8iG2Zpv4WGMNQgzMH1NRojsG9eZ08eWdb/JEcN7ONLSie3bIpas25f+Qfb5AXFDMvK46Fx1JD+Ko5grpCOGlMtbEdFNVlBLwFMIa7u9i/pcbXjRo5GqfrILdfi2/KBuvldQKBgQDNtmr29o6Bk7QRGr9vn8vurDcTOh+hisFuaoaHDCF/dTgf3jWw4Hpr/GAnTbAnVLMHfmK00F4KjL4OQi5tNfLGf4o4uXM5fobQTgDwJeMM0mu8P8c+gwwu1c88H+PdDitYfel/BPjMsBzS6M33q13dXqW60IGGditzq4Cm40YRRwKBgQDHsnloQAbYz5Pqp5XakNGLUEBiCdpP1UxDExCTfFqgw2I25Rzg3SondkZxQg6FMltQZmFoqJfVLIKtFs74Rg3SwfYyhEg8ME/0ey6HMlxx9t6j+VE0K2jtwULFu/peoaT1+r3tX3NJ3qyfwEfGj9k9s1EM3L+ZDor/1BSvzPz/TwKBgQDNmK+3PByZbOHe5njGO1M6q2wwDztl3KQOkvD5MCqalLEPSKsoqHIyUv5WydJwvLphlvNX+5jBuoCJB6QXCoAl23ptzwtWPxrGPe+6FGOkgPmkH6om1BrtBEwmr0ixumOgAdfIV1PCX5GvSXXxPpiFkv7Yg2Ow25H5/UKLiUKSKQKBgEuZ8sKX8r+kHmCo31+mkd4HHMjA6ChvHPcLwavEWV8heQ5FQOCUekAEU/jOxEaC4PUYPWps4UZwGmzDBMoTY2pVygon8FzxAWQSOnQuibcPegxj9+0jPg88qjXHy6qF0bjaG4tBJplhtsKn/cRbcygbueR/sf2U9uSRet9vYJybAoGAU0bN2ime2mkVEbejeZPVqEsLp5jhgLtNkVzUm0wF5e3JFosc17mtbHr/cgWBdlphUmhJdGWR/pbLDXcsn8fD8fE4evlDZ7jPG5G7kKYIOeXrDiikWDPpjvSZaEVLJKXI/h8xKx+o8D9BDcHwRfCjfs0WrozttFXZ4/zl06+R02o='
		// 设置私钥
		signStr.setPrivateKey('-----BEGIN RSA PRIVATE KEY-----\n' + privKey + '\n-----END RSA PRIVATE KEY-----');
		// 对数据进行签名
		var sign = signStr.sign(plainText, CryptoJS.SHA256, 'sha256')
		return sign;
	}
	
}
