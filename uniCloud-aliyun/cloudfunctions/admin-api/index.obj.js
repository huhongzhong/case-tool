// 云对象教程: https://uniapp.dcloud.net.cn/uniCloud/cloud-obj
// jsdoc语法提示教程：https://ask.dcloud.net.cn/docs/#//ask.dcloud.net.cn/article/129
const db = uniCloud.database({
	provider: 'aliyun',
	spaceId: 'mp-7985b918-c382-4fff-8cf4-2ec098628134'
})
module.exports = {
	_before: function() { // 通用预处理器

	},
	/**
	 * 添加注册码
	 * @param {string} 
	 * @returns {object} 返回值描述
	 * type 1 7天 2 30天 3 999天
	 */
	async addPwd(param) {
		console.log(param)
		let result = null
		let userId = ''
		let time = 0
		if (param.type == 1) {
			userId = '7天'
			time = new Date().getTime() + (1000 * 60 * 60 * 24 * 7)
		} else if (param.type == 2) {
			userId = '30天'
			time = new Date().getTime() + (1000 * 60 * 60 * 24 * 30)
		} else if (param.type == 3) {
			userId = '999天'
			time = new Date().getTime() + (1000 * 60 * 60 * 24 * 999)
		}

		const data = await db.collection('pwd-list').add({
			"user_id": userId,
			"end_date": time,
			"uuid": "",
			"is_menu": "YES"
		})
		console.log(data)
		// 参数校验，如无参数则不需要
		if (data.updated == '0') {
			return {
				code: 1002,
				data: null,
				msg: '无效的注册码或注册码已绑定'
			}
		}
		// 业务逻辑
		// 返回结果
		return {
			code: 200,
			data: data,
			msg: '成功'
		}
	},
	async getList(param) {
		let userId = ''
		if (param.type == 1) {
			userId = '7天'
		} else if (param.type == 2) {
			userId = '30天'
		} else if (param.type == 3) {
			userId = '999天'
		}
		let data = await db.collection('pwd-list').where({
			"user_id": userId,
			"uuid": ''
		}).get()
		return {
			code: 200,
			data: data.data,
			msg: '成功'
		}
	},
	async getPwd(id) {
		console.log('参数', id)
		let data = await db.collection('pwd-list').where({
			_id: id,
		}).get()
		return {
			code: 200,
			data: data.data,
			msg: '成功'
		}
	},
	async alterPwdTime(id, day) {
		let data = await db.collection('pwd-list').where({
			_id: id,
		}).get()
		console.log(data)
		if (data.data && data.data.length > 0) {
			let param  = data.data[0]
			let res = await db.collection('pwd-list').where({
				_id: id
			}).update({
				end_date: param.end_date + ((1000 * 60 * 60 * 24 )*day)
			})
			console.log(res)
			return {
				code: 200,
				data: res.data,
				msg: '成功'
			}
		} else {
			return {
				code: 100,
				data: [],
				msg: '失败'
			}
		}

	},
	async onPwdReset(id) {
		let data = await db.collection('pwd-list').where({
			_id: id,
		}).update({
			uuid: ""
		})
		return {
			code: 200,
			data: {},
			msg: '成功'
		}
	}
}