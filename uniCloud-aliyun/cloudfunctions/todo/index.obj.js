// 云对象教程: https://uniapp.dcloud.net.cn/uniCloud/cloud-obj
// jsdoc语法提示教程：https://ask.dcloud.net.cn/docs/#//ask.dcloud.net.cn/article/129
const db = uniCloud.database({
	provider: 'aliyun',
	spaceId: 'mp-7985b918-c382-4fff-8cf4-2ec098628134'
})
var endTime = 10
var interval = null
var isEnd = false
module.exports = {

	_before: function() { // 通用预处理器
		this.time= 0
	},
	/**
	 * 注册
	 * @param {string} 
	 * @returns {object} 返回值描述
	 */
	async registry(param) {
		console.log(param)
		let result = null
		
		const pwdInfo = await db.collection('pwd-list').where({
			_id: param.pwd,
			uuid: ''
		}).get()
		let time = 0
		if(pwdInfo.data&&pwdInfo.data.length>0){
			if(pwdInfo.data[0].user_id=="7天"){
				time =  new Date().getTime() + (1000 * 60 * 60 * 24 * 7)
			}else if(pwdInfo.data[0].user_id=="30天"){
				time =  new Date().getTime() + (1000 * 60 * 60 * 24 * 30)
			}else if(pwdInfo.data[0].user_id=="999天"){
				time =  new Date().getTime() + (1000 * 60 * 60 * 24 * 999)
			}
		}else{
			return {
				code: 1002,
				data: null,
				msg: '无效的注册码或注册码已绑定'
			}
		}
		
		const data = await db.collection('pwd-list').where({
			_id: param.pwd,
			uuid: ''
		}).update({
			uuid: param.uuid,
			bind_date: new Date().getTime(),
			end_date:time
		})
		console.log(data)
		// 参数校验，如无参数则不需要
		if (data.updated == '0') {
			return {
				code: 1002,
				data: null,
				msg: '无效的注册码或注册码已绑定'
			}
		}
		// 业务逻辑
		// 返回结果
		return {
			code: 200,
			data: {},
			msg: '成功'
		}
	},
	// 获取注册码 验证是否过期
	async getRegistryCode(pwd) {
		const dbCmd = db.command // 取指令
		// 判断时间大于
		let res = await db.collection('pwd-list').where({
			_id: pwd,
			end_date: dbCmd.gt(new Date().getTime())
		}).get()
		if (res.data && res.data.length > 0) {
			return {
				code: 200,
				data: res.data,
				msg: '成功'
			}
		} else {
			return {
				code: 1002,
				data: null,
				msg: '注册码已过期'
			}
		}
	},
	// 获取当前合成详情 开始合成
	getDetail(id) {
		let num = 2000
		isEnd = false
		interval =  setInterval(r=>{
			num = num-1000
			this.time = new Date().getTime()
			console.log(num)
			console.log('时间',this.time)
			if(num<=0){
				console.log('去执行任务')
				clearInterval(interval)
				getCL(0,id,'')
			}
		},500)
		
		return {
			code:200
		}
	},
	onEnd(){
		console.log('结束')
		console.log(interval)
		isEnd = true
		clearInterval(interval)
		return {
			code:200
		}
	},
	getBase64(obj) {
		let encr = new Buffer(JSON.stringify(obj)).toString('base64')
		console.log(encr)
		return encr
	},

}
