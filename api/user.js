import httpUtils from '../util/httpUtils.js'
//登录接口
export function login(param){
	return httpUtils.request('post','/appusercenter/appApi/appUserPhonePasswordLogin',param)
}
export function codeLogin(param){
	return httpUtils.request('post','/np-app-user/appApi/login/phoneVerifyCodeLogin',param)
}
// 登录状态
export function getUserInfo(param){
	return httpUtils.request('post','/np-app-user/appApi/user/info',param)
}

// 获取商品列表
export function getGoodsList(param){
	return httpUtils.request('post','/np-collection/appApi/market/searchSeriesList',param)
}
// 获取商品寄售列表
export function getMarketRecommendGoodsList(param){
	return httpUtils.request('post','/waliangge/appApi/market/marketRecommendGoodsList',param)
}
// 快捷下单
export function createOrder(param){
	return httpUtils.request('post','/np-collection/appApi/order/create/item',param)
}
// 余额支付
export function blancePay(param){
	return httpUtils.request('post','/np-collection/appApi/order/pay/call',param)
}

// 获取活动列表
export function getActionList(param){
	return httpUtils.request('post','/np-collection/appApi/activity/list',param)
}
// 获取活动详情
export function getActionDetail(param){
	return httpUtils.request('post','/np-collection/appApi/merge/info',param)
}

// 查询材料
export function getItemList(param){
	return httpUtils.request('post','/np-collection/appApi/merge/itemList',param)
}
// 合成
export function sendmMerge(param){
	return httpUtils.request('post','/np-collection/appApi/merge/merge',param)
}
// 获取当前类型拥有的藏品
export function getTypeDataList(param){
	return httpUtils.request('post','/np-collection/appApi/collection/userSeriesCollectionList',param)
}
// 获取我的藏品
export function getUserHoldSeriesList(param){
	return httpUtils.request('post','/np-collection/appApi/series/getUserHoldSeriesList',param)
}
// 寄售
export function apiConsignment(param){
	console.log("开始",param)
	return httpUtils.request('post','/np-collection/appApi/collection/onSale',param)
}